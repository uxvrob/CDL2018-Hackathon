{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table width=60% >\n",
    "    <tr style=\"background-color: white;\">\n",
    "        <td><img src='https://www.creativedestructionlab.com/wp-content/uploads/2018/05/xanadu.jpg'></td>\n",
    "        <td><img src='https://www.creativedestructionlab.com/wp-content/uploads/2016/10/creative-destruction-lab.png' ></td>\n",
    "    </tr>\n",
    "</table>\n",
    "\n",
    "---\n",
    "\n",
    "<img src='https://raw.githubusercontent.com/XanaduAI/strawberryfields/master/doc/_static/strawberry-fields-text.png'>\n",
    "\n",
    "---\n",
    "\n",
    "<br>\n",
    "\n",
    "<center> <h1> State learning tutorial </h1></center>\n",
    "\n",
    "Now that we are more familiar with Strawberry Fields, let's have a go using the TensorFlow backend to do some quantum circuit optimization. This functionality is provided via the Tensorflow simulator backend. By leveraging Tensorflow, we have access to a number of additional funtionalities, including GPU integration, automatic gradient computation, built-in optimization algorithms, and other machine learning tools."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Background information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using the TensorFlow backend\n",
    "\n",
    "Using the TensorFlow backend is similar to using the Fock and Gaussian backend in the previous tutorials, however there are a couple of slight subtleties that need to be accounted for.\n",
    "\n",
    "We import Strawberry Fields as before, and create the quantum engine:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import strawberryfields as sf\n",
    "from strawberryfields.ops import *\n",
    "\n",
    "eng, q = sf.Engine(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When a circuit contains only numerical parameters, the Tensorflow simulator backend works the same as the other backends. However, with the Tensorflow backend, we have the additional option to use Tensorflow objects (e.g., `tf.Variable`, `tf.constant`, `tf.placeholder`, or `tf.Tensor`) for the parameters of quantum states, gates, and measurements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "alpha = tf.Variable(0.5)\n",
    "theta_bs = tf.constant(0.0)\n",
    "phi_bs = tf.sigmoid(0.0) # this will be a tf.Tensor object\n",
    "phi = tf.placeholder(tf.float32)\n",
    "\n",
    "with eng:\n",
    "    # States\n",
    "    Coherent(alpha)            | q[0]\n",
    "\n",
    "    # Gates\n",
    "    BSgate(theta_bs, phi_bs)   | (q[0], q[1])\n",
    "\n",
    "    # Measurements\n",
    "    MeasureHomodyne(phi)       | q[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To run a Strawberry Fields simulation with the Tensorflow backend, we need to specify `'tf'` as the backend argument when calling `eng.run()`. However, directly evaluating a circuit which contains Tensorflow objects using `eng.run()` will produce errors. The reason for this is that `eng.run()` tries, by default, to numerically evaluate any measurement result. But Tensorflow requires several extra ingredients to do this:\n",
    "\n",
    "* Numerical computations must be carried out using a `tf.Session`.\n",
    "* All `tf.Variable` objects must be initialized within this `tf.Session` (the initial values are supplied when creating the variables).\n",
    "* Numerical values must be provided for any `tf.placeholder` objects using a feed dictionary (`feed_dict`).\n",
    "\n",
    "The solution is to perform the simulation *symbolically*, using the keyword argument `eval=False`. The final state and the register q will both instead contain unevaluted Tensors. These Tensors can be evaluated numerically by running the `tf.Session` and supplying the desired values for any placeholders:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "state = eng.run('tf', cutoff_dim=7, eval=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eng, q = sf.Engine(2)\n",
    "\n",
    "with eng:\n",
    "    Dgate(alpha)         | q[0]\n",
    "    MeasureHomodyne(phi) | q[0]\n",
    "\n",
    "state = eng.run('tf', cutoff_dim=7, eval=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Measurement values and state properties/methods will now return unevaluated, symbolic tensors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "state_density_matrix = state.dm()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(state.dm())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "homodyne_meas = q[0].val"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(q[0].val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can evaluate these tensors by calling Tensorflow's `sess.run` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sess = tf.Session()\n",
    "sess.run(tf.global_variables_initializer())\n",
    "dm_x, meas_x = sess.run([state_density_matrix, homodyne_meas], feed_dict={phi: 0.0})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(meas_x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\" style=\"border: 0px; border-left: 3px solid #119a68; color: black; background-color: #daf0e9\">\n",
    "<p style=\"color: #119a68;\">**Note**</p>When being used as a numerical simulator (similar to the other backends), the Tensorflow backend creates temporary sessions in order to evaluate measurement results numerically.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Processing data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameters for Blackbird states, gates, and measurements may be more complex than just raw data or machine learning weights. These can themselves be the outputs from some learnable function like a neural network:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_ = tf.placeholder(tf.float32, shape=(2,1))\n",
    "weights = tf.Variable([[0.1,0.1]])\n",
    "bias = tf.Variable(0.0)\n",
    "\n",
    "NN = tf.sigmoid(tf.matmul(weights, input_) + bias)\n",
    "NNDgate = Dgate(NN)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use the `sf.convert()` decorator to allow arbitrary processing of measurement results with the Tensorflow backend:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@sf.convert\n",
    "def sigmoid(x):\n",
    "    return tf.sigmoid(x)\n",
    "\n",
    "with eng:\n",
    "    MeasureX             | q[0]\n",
    "    Dgate(sigmoid(q[0])) | q[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Working with batches\n",
    "\n",
    "It is common in machine learning to process data in batches. Strawberry Fields supports both unbatched and batched data when using the Tensorflow backend. Unbatched operation is the default behaviour (shown above). To enable batched operation, you should provide an extra batch_size argument when calling `eng.run()`, e.g.,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run simulation in batched-processing mode\n",
    "batch_size = 3\n",
    "eng, q = sf.Engine(2)\n",
    "\n",
    "with eng:\n",
    "    Dgate(tf.Variable([0.1] * batch_size)) | q[0]\n",
    "\n",
    "state = eng.run('tf', cutoff_dim=7, eval=False, batch_size=batch_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parameters supplied to a circuit in batch-mode operation can either be scalars or vectors (of length batch_size). Scalars are **automatically broadcast** over the batch dimension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alpha = tf.Variable([0.5] * batch_size)\n",
    "theta = tf.constant(0.0)\n",
    "phi = tf.Variable([0.1, 0.33, 0.5])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Measurement results will be returned as Tensors with shape `(batch_size,)`. We can picture batch-mode operation as simulating multiple circuit configurations at the same time. Combined with appropriate parallelized hardware like GPUs, this can result in significant speedups compared to serial evaluation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "state.ket().shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: variational quantum circuit optimization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A key element of machine learning is optimization. We can use Tensorflow’s automatic differentiation tools to optimize the parameters of variational quantum circuits. In this approach, we fix a circuit architecture where the states, gates, and/or measurements may have learnable parameters associated with them. We then define a loss function based on the output state of this circuit.\n",
    "\n",
    "\n",
    "<div class=\"alert alert-info\" style=\"border: 0px; border-left: 3px solid #31708f; color: black; background-color: #d9edf7\">\n",
    "<p style=\"color: #31708f;\">**Exercise**</p>\n",
    "\n",
    "Complete the Strawberry Fields program below, to optimize a Dgate to produce an output with the largest overlap with the Fock state $n=1$.\n",
    "\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#!/usr/bin/env python3\n",
    "import strawberryfields as sf\n",
    "from strawberryfields.ops import *\n",
    "import tensorflow as tf\n",
    "\n",
    "# create a one mode engine\n",
    "eng, q =\n",
    "\n",
    "# The displacement parameter is a TF variable\n",
    "alpha = tf.Variable(0.1)\n",
    "\n",
    "# Apply a Dgate with this parameter to the qumode\n",
    "# and run the engine using the Tensorflow backend\n",
    "# in symbolic mode\n",
    "\n",
    "with eng:\n",
    "\n",
    "\n",
    "# calculate the probability of the \n",
    "# resulting state being in Fock state |1>\n",
    "prob = state.\n",
    "\n",
    "\n",
    "# The loss function:\n",
    "# Loss is the probability for the Fock state n=1.\n",
    "# Note that we add a negative sign since\n",
    "# we wish to *maximize* the probability overlap\n",
    "loss = -prob  \n",
    "\n",
    "# Set up optimization\n",
    "optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)\n",
    "minimize_op = optimizer.minimize(loss)\n",
    "\n",
    "# Create Tensorflow Session and initialize variables\n",
    "sess = tf.Session()\n",
    "sess.run(tf.global_variables_initializer())\n",
    "\n",
    "# Carry out optimization\n",
    "for step in range(50):\n",
    "    prob_val, alpha_val, _ = sess.run([prob, alpha, minimize_op])\n",
    "    print(\"Value at step {}: {}\".format(step, prob_val, alpha_val))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" style=\"border: 0px; border-left: 3px solid #31708f; color: black; background-color: #d9edf7\">\n",
    "<p style=\"color: #31708f;\">**Exercise**</p>\n",
    "\n",
    "Plot the Wigner function for the single photon state, and compare it to the optimized state above.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, you need to numerically evaluate the density matrix of the state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rho = "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following Wigner function is provided for convenience."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Wigner function for plotting purposes\n",
    "def wigner(rho):\n",
    "    import copy\n",
    "    # Domain parameter for Wigner function plots\n",
    "    l = 5.0\n",
    "    cutoff = rho.shape[0]\n",
    "\n",
    "    # Creates 2D grid for Wigner function plots\n",
    "    x = np.linspace(-l, l, 100)\n",
    "    p = np.linspace(-l, l, 100)\n",
    "\n",
    "    Q, P = np.meshgrid(x, p)\n",
    "    A = (Q + P * 1.0j) / (2 * np.sqrt(2 / 2))\n",
    "\n",
    "    Wlist = np.array([np.zeros(np.shape(A), dtype=complex) for k in range(cutoff)])\n",
    "\n",
    "    # Wigner function for |0><0|\n",
    "    Wlist[0] = np.exp(-2.0 * np.abs(A) ** 2) / np.pi\n",
    "\n",
    "    # W = rho(0,0)W(|0><0|)\n",
    "    W = np.real(rho[0, 0]) * np.real(Wlist[0])\n",
    "\n",
    "    for n in range(1, cutoff):\n",
    "        Wlist[n] = (2.0 * A * Wlist[n - 1]) / np.sqrt(n)\n",
    "        W += 2 * np.real(rho[0, n] * Wlist[n])\n",
    "\n",
    "    for m in range(1, cutoff):\n",
    "        temp = copy.copy(Wlist[m])\n",
    "        # Wlist[m] = Wigner function for |m><m|\n",
    "        Wlist[m] = (2 * np.conj(A) * temp - np.sqrt(m)\n",
    "                    * Wlist[m - 1]) / np.sqrt(m)\n",
    "\n",
    "        # W += rho(m,m)W(|m><m|)\n",
    "        W += np.real(rho[m, m] * Wlist[m])\n",
    "\n",
    "        for n in range(m + 1, cutoff):\n",
    "            temp2 = (2 * A * Wlist[n - 1] - np.sqrt(m) * temp) / np.sqrt(n)\n",
    "            temp = copy.copy(Wlist[n])\n",
    "            # Wlist[n] = Wigner function for |m><n|\n",
    "            Wlist[n] = temp2\n",
    "\n",
    "            # W += rho(m,n)W(|m><n|) + rho(n,m)W(|n><m|)\n",
    "            W += 2 * np.real(rho[m, n] * Wlist[n])\n",
    "\n",
    "    return Q, P, W / 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(111, projection=\"3d\")\n",
    "X, P, W = wigner(rho)\n",
    "ax.plot_surface(X, P, W, cmap=\"RdYlGn\", lw=0.5, rstride=1, cstride=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Layers and state learning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For arbitrary state preparation using optimization, we need to make use of a quantum circuit with a layer structure that is **universal** - that is, by 'stacking' the layers, we can guarantee that we can produce *any* CV state. Therefore, the architecture we choose must consist of layers with each layer containing parameterized Gaussian *and* non-Gaussian gates. **The non-Gaussian gates provide both the nonlinearity and the universality of the model.**\n",
    "\n",
    "![layer](https://i.imgur.com/NEsaVIX.png)\n",
    "\n",
    "\n",
    "<div class=\"alert alert-success\" style=\"border: 0px; border-left: 3px solid #119a68; color: black; background-color: #daf0e9\">\n",
    "<p style=\"color: #119a68;\">**Reference**</p>Killoran, N., Bromley, T. R., Arrazola, J. M., Schuld, M., Quesada, N., & Lloyd, S. (2018). \"Continuous-variable quantum neural networks.\" [arXiv:1806.06871.](https://arxiv.org/abs/1806.06871)\n",
    "</div>\n",
    "\n",
    "First, we must define the **hyperparameters** of our layer structure:\n",
    "\n",
    "* `cutoff`: the Fock space truncation we will use in the optimization. The TensorFlow backend will perform operations in this truncated Fock space when performing the optimization.\n",
    "\n",
    "\n",
    "* `depth`: The number of layers in our quantum circuit. As a general rule, increasing the number of layers (and thus, the number of parameters we are optimizing over) increases the optimizers chance of finding a reasonable local minimum in the optimization landscape.\n",
    "\n",
    "\n",
    "* `reps`: the number of steps in optimization routine performing gradient descent\n",
    "\n",
    "Some other optional hyperparameters include:\n",
    "\n",
    "* `penalty_strength`: this allows us to *penalize* deviations from a trace of 1 in the cost function. This helps ensure that the optimization process never results in operations that transform the state beyond the truncated Fock space.\n",
    "\n",
    "\n",
    "* The standard deviation of initial parameters. Note that we make a distinction between the standard deviation of *passive* parameters (those that preserve photon number when changed, such as phase parameters), and *active* parameters (those that introduce or remove energy from the system when changed)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cutoff dimension\n",
    "cutoff = 6\n",
    "\n",
    "# Number of layers\n",
    "depth = 8\n",
    "\n",
    "# Number of steps in optimization routine performing gradient descent\n",
    "reps = 1000\n",
    "\n",
    "# Penalty coefficient to ensure the state is normalized\n",
    "penalty_strength = 100\n",
    "\n",
    "# Standard deviation of initial parameters\n",
    "passive_sd = 0.1\n",
    "active_sd = 0.001"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The layer parameters\n",
    "\n",
    "We use TensorFlow to create the variables corresponding to the gate parameters. Note that each variable has shape `[depth]`, with each individual element representing the gate parameter in layer $i$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# squeeze gate\n",
    "sq_r = tf.Variable(tf.random_normal(shape=[depth], stddev=active_sd))\n",
    "sq_phi = tf.Variable(tf.random_normal(shape=[depth], stddev=passive_sd))\n",
    "\n",
    "# displacement gate\n",
    "d_r = tf.Variable(tf.random_normal(shape=[depth], stddev=active_sd))\n",
    "d_phi = tf.Variable(tf.random_normal(shape=[depth], stddev=passive_sd))\n",
    "\n",
    "# rotation gates\n",
    "r1 = tf.Variable(tf.random_normal(shape=[depth], stddev=passive_sd))\n",
    "r2 = tf.Variable(tf.random_normal(shape=[depth], stddev=passive_sd))\n",
    "\n",
    "# kerr gate\n",
    "kappa = tf.Variable(tf.random_normal(shape=[depth], stddev=active_sd))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The layer function\n",
    "\n",
    "Now, we can create a function to define the $i$th layer, acting on qumode `q`. This allows us to simply call this function in a loop later on when we build our circuit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# layer architecture\n",
    "def layer(i, q):\n",
    "    Rgate(r1[i]) | q\n",
    "    Sgate(sq_r[i], sq_phi[i]) | q\n",
    "    Rgate(r2[i]) | q\n",
    "    Dgate(d_r[i], d_phi[i]) | q\n",
    "    Kgate(kappa[i]) | q"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constructing the circuit\n",
    "\n",
    "Now that we have defined our gate parameters and our layer structure, we can construct our variational quantum circuit. Note that, as before, we use the TensorFlow backend with `eval=False`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start SF engine\n",
    "engine, q = sf.Engine(1)\n",
    "\n",
    "# Apply circuit of layers with corresponding depth\n",
    "with engine:\n",
    "    for k in range(depth):\n",
    "        layer(k, q[0])\n",
    "\n",
    "# Run engine\n",
    "state = engine.run('tf', cutoff_dim=cutoff, eval=False)\n",
    "ket = state.ket()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Performing the optimization\n",
    "\n",
    "$$\\newcommand{ket}[1]{\\left|#1\\right\\rangle}$$\n",
    "\n",
    "With the TensorFlow backend calculating the resulting state of the circuit symbolically, we can use TensorFlow to optimize the gate parameters to minimize the cost function we specify. With state learning, the cost function we often wish to minimize is the **fidelity of the output state $\\ket{\\psi}$ with some target state $\\ket{\\psi_t}$**. This is defined as the overlap between the two states:\n",
    "\n",
    "$$ \\text{fidelity} = \\left|\\left\\langle{\\psi}\\mid{\\psi_t}\\right\\rangle\\right|^2$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<div class=\"alert alert-info\" style=\"border: 0px; border-left: 3px solid #31708f; color: black; background-color: #d9edf7\">\n",
    "<p style=\"color: #31708f;\">**Exercise**</p>\n",
    "\n",
    "Define a target state you wish to learn using the variational quantum circuit below, and run the optimization. A good starting example is the single photon state $\\ket{1}$.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "target_state = "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using this target state, we calculate the fidelity with the state exiting the variational circuit. Note that we must use TensorFlow functions to manipulate this data, as were are working with symbolic variables!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fidelity = tf.abs(tf.reduce_sum(tf.conj(ket) * target_state)) ** 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use this to construct our cost function. We can also include the penalty on the trace, to ensure the trace of the system remains close to 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Objective function to minimize\n",
    "cost = (1-fidelity) + penalty_strength*tf.abs(1-state.trace())\n",
    "tf.summary.scalar('cost', cost)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that the cost function is defined, we can define and run the optimization. Below, we choose the Adam optimizer that is built into TensorFlow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Using Adam algorithm for optimization\n",
    "optimiser = tf.train.AdamOptimizer()\n",
    "min_cost = optimiser.minimize(cost)\n",
    "\n",
    "# Begin Tensorflow session\n",
    "session = tf.Session()\n",
    "session.run(tf.global_variables_initializer())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then loop over all repetitions, storing the best predicted fidelity value. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fid_progress = []\n",
    "best_fid = 0\n",
    "\n",
    "# Run optimization\n",
    "for i in range(reps):\n",
    "\n",
    "    # one repitition of the optimization\n",
    "    _, cost_val, fid_val, ket_val = session.run([min_cost, cost, fidelity, ket])\n",
    "\n",
    "    # Stores fidelity at each step\n",
    "    fid_progress.append(fid_val)\n",
    "    \n",
    "    if fid_val > best_fid:\n",
    "        # store the new best fidelity and best state\n",
    "        best_fid = fid_val\n",
    "        best_state = ket_val\n",
    "\n",
    "    # Prints progress at every 10 reps\n",
    "    if i % 10 == 0:\n",
    "        print(i, fid_val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<div class=\"alert alert-info\" style=\"border: 0px; border-left: 3px solid #31708f; color: black; background-color: #d9edf7\">\n",
    "<p style=\"color: #31708f;\">**Exercise**</p>\n",
    "\n",
    "Plot the Wigner function of the learnt state and the target state.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell computes the density matrix $\\rho = \\left|\\psi\\right\\rangle \\left\\langle\\psi\\right|$ of a pure state vector. This can be used in the Wigner function defined in the previous section."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rho = np.outer(best_state, best_state.conj())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  },
  "toc": {
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
